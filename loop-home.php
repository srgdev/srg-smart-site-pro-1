<?php
/**
 * Home page template loop - returns only 3 posts, instead of blog default.
 */
?>

<h1><?php echo strtoupper(get_the_title(get_option('page_for_posts'))); ?></h1>
    
<?php $args = array('post_type' => 'post', 'posts_per_page' => '3'); ?>
<?php $news = new WP_Query($args); ?>
<?php if($news->have_posts()): while($news->have_posts()): $news->the_post(); ?>

<div class="post">
	<?php if(has_post_thumbnail()): ?>
		<div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
	<?php endif; ?>
    <div class="postContent">
    	<h3><?php the_title(); ?></h3>
        <p><?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">[…]</a></p>
    </div>
    <br class="clear">
    <div class="postFooter">
    	<div class="postDate"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?></div>
        <?php create_tag_list(); ?>
        <br class="clear">
    </div>
</div>
<?php endwhile; endif; ?>