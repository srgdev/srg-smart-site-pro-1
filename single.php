<?php
/**
 * The Template for displaying all single posts.
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
        <?php if(is_active_sidebar('blog-sidebar')): ?>
            <?php get_sidebar('blog'); ?>
        <?php endif; ?>
        
        <div id="content">
          <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <div class="post">
            <?php if(has_post_thumbnail()): ?>
                <div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
            <?php endif; ?>
            <div class="postContent">
                <h3><?php the_title(); ?></h3>
                <?php the_content(); ?>
            </div>
            <br class="clear">
            <div class="postFooter">
                <div class="postDate"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?></div>
                <?php create_tag_list(); ?>
                <br class="clear">
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>