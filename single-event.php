<?php
/**
 * The Template for displaying all single events.
 */

get_header(); ?>

<div id="contentRow" class="row">
    <div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
        <?php if(is_active_sidebar('blog-sidebar')): ?>
            <?php get_sidebar('blog'); ?>
        <?php endif; ?>
        
        <div id="content">
          <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <div class="post">
            <?php if(has_post_thumbnail()): ?>
                <div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
            <?php endif; ?>
            <div class="postContent">
                <h3><?php the_title(); ?></h3>
                <div class="eventDate"><i class="fa fa-clock-o"></i> <?php echo date('F j, Y', strtotime(get_field('start_date'))); ?></div>
                <?php the_content(); ?>
            </div>
            <br class="clear">
            <div class="postFooter">
                <?php create_tag_list(); ?>
                <br class="clear">
            </div>
        </div>
        <?php endwhile; ?>
        <?php endif; ?>
        </div> <!-- End Content -->
    
        <br class="clear">
    
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>