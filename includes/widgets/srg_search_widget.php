<?php

/**
* SRG Search Widget - Generate a search form for current post type
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Search_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Search_Widget', 'SRG Search Widget', array( 'description' => 'Search form Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		?>
        <?php echo $args['before_widget']; ?>
        <div class="searchItem">
            <form method="get" action="<?php bloginfo('url'); ?>">
                <input name="s" type="text" placeholder="SEARCH <?php echo get_post_type() ? strtoupper(get_post_type()) : strtoupper($_GET['post_type']); ?>S" class="field" value="<?php echo $_GET['s']; ?>" />
                <input type="hidden" name="post_type" value="<?php echo get_post_type() ? get_post_type(): $_GET['post_type']; ?>" />
                <input name="submit" type="submit" value="&#xf002;" class="submit">
                <br class="clear">
            </form>
        </div>
        <?php echo $args['after_widget']; ?>
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {

		// Widget admin form
		?>
		<p>
		No options for this widget.
		</p>
        
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
		
	}
	
}

// Register and load the widget
function srg_search_widget_load() {
	register_widget( 'SRG_Search_Widget' );
}
add_action( 'widgets_init', 'srg_search_widget_load' );

?>