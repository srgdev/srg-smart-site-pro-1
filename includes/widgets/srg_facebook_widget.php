<?php

/**
* SRG Facebook Widget - returnsfacebook likes iframe
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Facebook_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Facebook_Widget', 'SRG Facebook Widget', array( 'description' => 'Facebook Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$url = rawurlencode(get_theme_mod('srg_theme_facebook'));
		?>
		<?php echo $args['before_widget']; ?>
		<div class="socWidget">
        	<iframe src="https://www.facebook.com/plugins/likebox.php?href=<?php echo $url; ?>&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=168922799959916" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>
        </div>
        <?php echo $args['after_widget'];?>
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		// Widget admin form
		?>
		<p style="text-overflow: ellipsis;overflow: hidden">Facebook page url: 
		<?php if(get_theme_mod('srg_theme_facebook')):?>
		    <?php echo get_theme_mod('srg_theme_facebook'); ?>
	    <?php else: ?>
	        undefined
        <?php endif; ?>
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		return $instance;
	}

}

// Register and load the widget
function srg_facebook_widget_load() {
	register_widget( 'SRG_Facebook_Widget' );
}
add_action( 'widgets_init', 'srg_facebook_widget_load' );

?>