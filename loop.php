<?php
/**
 * The loop that displays regular posts.
 */
?>

<h1>
    <?php if (is_category()): ?>
        <?php printf( __( 'Category Archives: %s', 'srg' ), '' . single_cat_title( '', false ) . '' );?>
    <?php elseif (is_tag()): ?>
        <?php printf( __( 'Tag Archives: %s', 'srg' ), '' . single_tag_title( '', false ) . '' ); ?>
    <?php elseif(is_search()): ?>
        <?php printf( __( 'Search Results for: %s', 'srg' ), '' . get_search_query() . '' ); ?>
    <?php elseif(is_tax()): ?>
        <?php if(is_tax('event_category')): ?>
            Events Category: <?php $term = $wp_query->get_queried_object(); echo $term->name; ?>
        <?php elseif(is_tax('event_tag')): ?>
            Events Tag: <?php $term = $wp_query->get_queried_object(); echo $term->name; ?>
        <?php else: ?>
            <?php $term = $wp_query->get_queried_object(); echo $term->name; ?>
        <?php endif; ?>
    <?php else: ?>
        <?php if ( have_posts() ) the_post(); ?>
            <?php if ( is_day() ) : ?>
                <?php echo ucfirst(get_post_type()).' '; ?><?php printf( __( 'Archives: %s', 'srg' ), get_the_date() ); ?>
            <?php elseif ( is_month() ) : ?>
                <?php echo ucfirst(get_post_type()).' '; ?><?php printf( __( 'Archives: %s', 'srg' ), get_the_date('F Y') ); ?>
            <?php elseif ( is_year() ) : ?>
                <?php echo ucfirst(get_post_type()).' '; ?><?php printf( __( 'Archives: %s', 'srg' ), get_the_date('Y') ); ?>
            <?php else : ?>
                <?php if(get_post_type() == 'post'): ?>
                    <?php echo strtoupper(get_the_title(get_option('page_for_posts'))); ?>
                <?php else: ?>
                    <?php echo ucfirst(get_post_type()).'S'; ?>
                <?php endif; ?>
            <?php endif; ?>    
        <?php rewind_posts(); ?>
    <?php endif; ?>
</h1>
 
<?php if(have_posts()): while(have_posts()): the_post(); ?>

<div class="post">
    <?php if(has_post_thumbnail()): ?>
        <div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
    <?php endif; ?>
    <div class="postContent">
        <h3><?php the_title(); ?></h3>
        <?php if(get_post_type() == 'event'): ?>
            <div class="eventDate"><i class="fa fa-clock-o"></i> <?php echo date('F j, Y', strtotime(get_field('start_date'))); ?></div>
        <?php endif; ?>
        <p><?php echo get_the_excerpt(); ?> <a href="<?php the_permalink(); ?>">[…]</a></p>
    </div>
    <br class="clear">
    <div class="postFooter">
        <?php if(get_post_type() != 'event'): ?>
            <div class="postDate"><i class="fa fa-clock-o"></i> <?php the_time('F j, Y'); ?></div>
        <?php endif; ?>
        <?php create_tag_list(); ?>
        <br class="clear">
    </div>
</div>
<?php endwhile; ?>
<?php else : ?>
    <h3><?php _e( 'Nothing Found', 'twentyten' ); ?></h3>
    <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'srg' ); ?></p>
<?php endif; ?>
 
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
    <div id="loadMore">
    	<?php if($wp_query->get('paged') || $wp_query->get('paged') > 1): ?>
        <a class="loadPrev" href="<?php previous_posts(); ?>"><i class="fa fa-caret-square-o-left"></i> prev</a>
        <?php endif; ?>
        
        <?php if ($next_url = next_posts($wp_query->max_num_pages, false)): ?>
        <a class="loadNext" href="<?php echo $next_url; ?>">next <i class="fa fa-caret-square-o-right"></i></a>
        <?php endif;?>
        
        <br class="clear">
    </div>
<?php endif; ?>
